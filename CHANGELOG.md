# Changelog

## 0.3.0
2018-12-07
### Features
- added is_empty method

## 0.2.0
2017-12-29
### Features
- added constructor for empty UVec
- implemented Clone trait
